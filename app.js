import express from 'express';
import bodyParser from 'body-parser';
import mongoose from 'mongoose';
import bCrypt from 'bcrypt';

import routes from './app/routes';

const runApp = () => {

    const app = express();
    app.use(bodyParser.urlencoded({ extended: false })); // parse application/x-www-form-urlencoded
    app.use(bodyParser.json()); // parse application/json

    mongoose.connect('mongodb://localhost:27017/auth', {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false
    });

    const db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error:'));
    db.once('open', function() {
        console.log("we're connected!");
    });

    app.get('/', (req, res) => {
        const salt = bCrypt.genSaltSync(10);
        res.status(200).json({
            status: 200,
            response: bCrypt.hashSync('password', salt)
        });
    });

    app.use('/', routes);

    app.listen(3000, () => {
        console.log('App listening on port 3000!');
    });

};

module.exports = runApp;