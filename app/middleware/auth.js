import jwt from 'jsonwebtoken';

import config from '../../config/config';

const authChecker = (req, res, next) => {
    const authHeader = req.get('Authorization');
    if (!authHeader) {
        return res.status(401).json({message: "Token not provided"});
    }

    const token = authHeader.replace('Bearer ', '');
    try {
        const payload = jwt.verify(token, config.jwt.secret);
        if (payload.type !== 'access') {
            return res.status(401).json({message: "Invalid token"});
        }
    } catch (e) {
        if (e instanceof jwt.TokenExpiredError) {
            return res.status(401).json({message: "Token Expired"});
        }
        if (e instanceof jwt.JsonWebTokenError) {
            return res.status(401).json({message: "Invalid token"});
        }
    }

    next();
};

module.exports = authChecker;