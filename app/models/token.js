import mongoose from 'mongoose';

const TokenSchema = new mongoose.Schema({
    tokenId: String,
    userId: String
});

module.exports = mongoose.model('Token', TokenSchema);