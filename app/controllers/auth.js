import bCrypt from 'bcrypt';
import jwt from 'jsonwebtoken';

import config from '../../config/config';
import User from '../models/user';
import Token from '../models/token';
import authHelper from '../../helpers/auth.js';

const updateTokens = userId => {
    const accessToken = authHelper.generateAccessToken(userId);
    const refreshToken = authHelper.generateRefreshToken();

    return authHelper.replaceDbRefreshToken(refreshToken.id, userId)
        .then(() => {
            console.log("replaceDbRefreshToken");
            return {
            accessToken,
            refreshToken: refreshToken.token
        }});
};

const signIn = (req, res) => {
    console.log('signIn');
    const {email, password} = req.body;
    User.findOne({email})
        .exec()
        .then(user => {
            console.log(user);
            if (!user) {
                res.status(400).json({
                    message: "User does not exist"
                });
            }
            const isValid = bCrypt.compareSync(password, user.password);
            if (isValid) {
                // const token = jwt.sign(user._id.toString(), config.jwt.secret);
                // res.json({ token });
                updateTokens(user._id).then(tokens => res.json(tokens));
            } else {
                res.status(401).json({
                    message: "Invalid credentials"
                });
            }
        })
        .catch(err => {
            res.status(500).json({
                messsage: err.message
            });
        });
};

const refreshTokens = (req, res) => {
    const {refreshToken} = req.body;
    let payload;

    try {
        payload = jwt.verify(refreshToken, config.jwt.secret);
        if (payload.type !== 'refresh') {
            return res.status(400).json({message: "Invalid token"});
        }
    } catch (e) {
        if (e instanceof jwt.TokenExpiredError) {
            return res.status(400).json({message: "Token Expired"});
        } else if (e instanceof jwt.JsonWebTokenError) {
            return res.status(400).json({message: "Invalid token"});
        }
    }

    Token.findOne({tokenId: payload.id})
        .exec()
        .then(token => {
           if (token === null) {
               throw new Error('Invalid token!');
           }

           return updateTokens(token.userId);
        })
        .then(tokens => res.json(tokens))
        .catch(err => res.status(400).json({message: err.message}));
};

module.exports = {signIn, refreshTokens};