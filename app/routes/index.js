import {Router} from 'express';

import auth from '../controllers/auth';
import authMiddleware from '../middleware/auth';

const router = Router();

router.post('/signIn', auth.signIn);
router.post('/refresh', auth.refreshTokens);
router.post('/test', authMiddleware, (req, res) => {
    res.json({message: 'test Auth!!!'});
});

module.exports = router;